import psycopg2
import configparser
from psycopg2.extras import execute_values
import pyspark
import os
from sqlalchemy import create_engine
import pandas as pd



absolute_path = os.path.dirname(os.path.abspath(__file__))
jarLoc = f'{absolute_path}\\lib\\postgresql-42.7.3.jar'
configLoc = f'{absolute_path}\\conf\\pgsql.conf'
outdir = f'{absolute_path}\\archive'



configParser = configparser.RawConfigParser()   
configParser.read(configLoc)
host= configParser.get('db_info', 'host')
user= configParser.get('db_info', 'user')
password= configParser.get('db_info', 'password')
port= configParser.get('db_info', 'port')


def truncate_tbl(dbname, tbl_name):
    conn = psycopg2.connect(database=dbname, user=user, password=password, host=host, port=port)
    cursor = conn.cursor()
    cursor.execute(f"truncate table {tbl_name}")
    conn.commit()

def delete_records_and(dbname, tbl_name, **kwargs):
    conn = psycopg2.connect(database=dbname, user=user, password=password, host=host, port=port)
    cursor = conn.cursor()
    
    queryString = f'delete from {table_name} where true'
    for key, value in kwargs.items():
        if type(value) is int:
            queryString += f' and {key} = {value}'
        else:
            queryString += f' and {key} = \'{value}\''
    cursor.execute(queryString)
    conn.commit()

def delete_records_or(dbname, tbl_name, **kwargs):
    conn = psycopg2.connect(database=dbname, user=user, password=password, host=host, port=port)
    cursor = conn.cursor()
    
    queryString = f'delete from {table_name} where false'
    for key, value in kwargs.items():
        if type(value) is int:
            queryString += f' or {key} = {value}'
        else:
            queryString += f' or {key} = \'{value}\''
    cursor.execute(queryString)
    conn.commit()



def load_tbl(dbname, tbl_name, data):

    dataList = []
    # print(data)
    if type(data) is list:
        dataList = data
    elif type(data) is dict:

        for key in data: 
            dataList.append(data[key])
    
    if len(dataList) == 0 :
        return
    conn = psycopg2.connect(database=dbname, user=user, password=password, host=host, port=port)
    # print(conn)
    columns = dataList[0].keys()
    query = "INSERT INTO {} ({}) VALUES %s".format(tbl_name, ','.join(columns))
    values = [[value for value in row.values()] for row in dataList]


    cursor = conn.cursor()

    execute_values(cursor, query, values)
    conn.commit()

def tbl_to_parquet(dbname, tbl_name, date):
    spark = (pyspark.sql.SparkSession.builder.master("local")
            .appName("PySpark Shoptitans")
            .config("spark.jars", jarLoc)
            .getOrCreate())
    url = f"jdbc:postgresql://{host}:{port}/{dbname}?user={user}&password={password}"
    properties = {
        "user": user,
        "password": password,
        "driver": "org.postgresql.Driver"
    }

    df = (spark.read.format("jdbc")
    .option("url", url)
    .option("driver", "org.postgresql.Driver")
    .option("query", f"select * from {tbl_name} where yearmonday = {date}")
    .load())


    df.write.parquet(f"{outdir}/{tbl_name}_{date}.parquet")
    
    return

def tbl_to_df_pds(dbname, table_name):
    engine = psycopg2.connect(f"dbname='{dbname}' user='{user}' host='{host}' port='{port}' password='{password}'")
    df = pd.read_sql(f'select * from {table_name}', con=engine)
    return df

def tbl_to_df_pds(dbname, table_name, **kwargs):
    
    engine = psycopg2.connect(f"dbname='{dbname}' user='{user}' host='{host}' port='{port}' password='{password}'")
    queryString = f'select * from {table_name} where true'
    for key, value in kwargs.items():
        if type(value) is int:
            queryString += f' and {key} = {value}'
        else:
            queryString += f' and {key} = \'{value}\''
    df = pd.read_sql(queryString, con=engine)
    return df

def tbl_to_df_spark(dbname, table_name):
    url = f"jdbc:postgresql://{host}:{port}/{dbname}?user={user}&password={password}"
    df = (spark.read.format("jdbc")
    .option("url", url)
    .option("driver", "org.postgresql.Driver")
    .option("query", f"select * from {table_name}")
    .load())
    
    return df

def tbl_to_parquet(dbname, tbl_name):
    url = f"jdbc:postgresql://{host}:{port}/{dbname}?user={user}&password={password}"


    df = (spark.read.format("jdbc")
    .option("url", url)
    .option("driver", "org.postgresql.Driver")
    .option("query", f"select * from {tbl_name}")
    .load())

    try:
        df.write.parquet(f"{outdir}/{tbl_name}.parquet")
    except Exception as e:
        print(e)
    return

def load_from_parquet(filename):
    df = spark.read.parquet(f"{outdir}/{filename}")
    print(df.count())
    return df