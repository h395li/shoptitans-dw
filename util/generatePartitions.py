import datetime
import pytz
import sys
from dateutil.relativedelta import relativedelta

tz = pytz.timezone('America/Toronto')


def generate_ingestion_listing_partitions():
    date = None
    formatString = '%Y%m%d'
    sqlFile = open("sql/partition_ingestion_listings.txt", "a")  # append mode

    startDate = None
    try:
        startDate = int(sys.argv[1])
        print(f"taking {startDate} as starting date")
        
        
        year = startDate / 10000
        startDate %= 10000
        
        month = startDate / 100
        startDate %= 100
        day = startDate
        date = datetime.datetime(year, month, day)
    except:
        date = datetime.datetime.today().astimezone(tz)
        startDate = int(f"{date.year}{date.month}{date.day}")

        print(f"taking {startDate} as starting date")


    for i in range(365): 
        currdate = date + datetime.timedelta(days=i)
        day = currdate.strftime(formatString)

        partition = f"CREATE TABLE ingestion_listings_{day} PARTITION OF ingestion_listings FOR VALUES in ({day});"

        sqlFile.write( partition + "\n")
        
        
        
def generate_dm_offer_listing_partitions():       
    formatStringDay = '%Y%m01'

    formatStringMon = '%Y%m'

    sqlFile = open("sql/partition_fact_offerlistings.txt", "a")  # append mode

    date = datetime.datetime.today().astimezone(tz)
    # startDate = int(f"{date.year}{date.month}{date.day}")
    prev = date + relativedelta(months=-1)
    for i in range(24): 
        
        currdate = date + relativedelta(months=i)
        month = prev.strftime(formatStringMon)

        
        partition = f"CREATE TABLE fact_offer_listings_{month} PARTITION OF fact_offer_listings FOR VALUES From ({prev.strftime(formatStringDay)}) to ({currdate.strftime(formatStringDay)});"
        prev = currdate

        sqlFile.write( partition + "\n")
        
generate_dm_offer_listing_partitions()