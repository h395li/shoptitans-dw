import schedule
import time
from ingestion_dataload_listing import load_ingestion_listings
import logging
import os
import datetime
import pytz


tz = pytz.timezone('America/Toronto')


absolute_path = os.path.dirname(os.path.abspath(__file__))

def main():
    schedule.every(1).minutes.do(load_ingestion_listings)
    # schedule.every().hour.do(job)
    # schedule.every(7).day.do(load_ingestion_items)

    while 1:
        schedule.run_pending()
        time.sleep(1)
    


def minute_load():
    
    date = datetime.datetime.today().astimezone(tz)
    str = date.strftime("%Y%m%d%M")
    logging.basicConfig(filename=f'{absolute_path}\\log\\ingestion_listing_{str}.log', level=logging.INFO)
    load_ingestion_listings()
    
if __name__ == '__main__':
    main()