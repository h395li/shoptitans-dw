drop table if exists fact_offer_listings;

create table fact_offer_listings ( 
 item_key integer,
 date_key integer,
 tod_key integer,
 goldQty integer,
 gemsQty integer,
 goldPrice integer,
 gemsPrice integer
)partition by range(date_key);

CREATE INDEX fact_offer_listings_item_key 
ON fact_offer_listings (item_key);
CREATE INDEX fact_offer_listings_date_key 
ON fact_offer_listings (date_key);
CREATE INDEX fact_offer_listings_tod_key 
ON fact_offer_listings (tod_key);

-- exception table, keep same metrics as ingestion table, so we can easily reload these failing records
drop table if exists fact_offer_listings_excep;

create table fact_offer_listings_excep ( 
 date_key integer,
 tod_key integer,
 uid varchar(255),
 goldQty integer,
 gemsQty integer,
 goldPrice integer,
 gemsPrice integer
)
