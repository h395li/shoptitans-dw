drop table if exists ingestion_listings;

create table ingestion_listings ( 
 uid varchar(255),
 tType varchar(255),
 tag1 varchar(255),
 tag2 varchar(255),
 tag3 varchar(255),
 goldQty integer,
 gemsQty integer,
 tier integer,
 "orders" integer,
 goldPrice integer,
 gemsPrice integer,
 createdAt timestamp,
 retrievedAt timestamp,
 yearmonday integer
) partition by list(yearmonday);