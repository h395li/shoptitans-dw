drop table if exists dim_item;

create table dim_item
(
item_key SERIAL PRIMARY KEY,
uid varchar(255),
code	varchar(255),
quality varchar(255),
name_en varchar(255),
name_zh varchar(255),
type	varchar(255),
xp	integer,
craftXp	integer,
value	integer,
value125 integer,
tradeMaxValue	integer,
favor	integer,
time	integer,
atk	integer,
def	integer,
hp	integer,
eva	integer,
crit	integer,
excl	varchar(255),
tier	integer,
worker1	varchar(255),
worker2	varchar(255),
worker3	varchar(255),
resource1	varchar(255),
r1Qty	integer,
resource2	varchar(255),
r2Qty	integer,
resource3	varchar(255),
r3Qty	integer,
component1	varchar(255),
c1Qty	integer,
c1Tags	varchar(255),
component2	varchar(255),
c2Qty	integer,
c2Tags	varchar(255),
upgrade1	varchar(255),
upgrade2	varchar(255),
upgrade3	varchar(255),
upgrade4	varchar(255),
upgrade5	varchar(255),
supgrade1	varchar(255),
supgrade2	varchar(255),
supgrade3	varchar(255),
suCost	integer,
elementAffinity	varchar(255),
spiritAffinity	varchar(255),
discount	integer,
surcharge	integer,
suggest	integer,
speedup	integer,
releaseAt	varchar(255)
);



CREATE INDEX dim_item_uid
ON dim_item (uid);



ALTER SEQUENCE
dim_item_item_key_seq RESTART WITH 1;

insert into dim_item (item_key, uid, code, name_en) values (0, 'na', 'na', 'Not Available');
