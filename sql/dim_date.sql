drop table if exists dim_date;

create table dim_date
(
date_key integer PRIMARY KEY,
"year" integer,
"month" integer,
"day" integer,
"event" varchar(255)
);



