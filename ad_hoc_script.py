from dbutils import *
from ingestion_dataload_listing import *
from ingestion_dataload_item import *
from dm_dataload_dim_item import *
from dm_staticload_dim_time import *
from fact_dataload_fact_listing import *
import pyspark
import pandas as pd

from enum import Enum

# tbl_to_parquet("ingestion_shoptitans", "ingestion_listings")
formatStringDate = '%Y%m%d'
formatStringtod = '%H%M'
load_fact_offerlisting(20240628)
# listingdf = tbl_to_df_pds("ingestion_shoptitans", "ingestion_listings", yearmonday=20240628, tType='o')

# listingdf["date_key"] = listingdf["createdat"].apply(lambda x : x.strftime(formatStringDate) )
# listingdf["tod_key"] = listingdf["createdat"].apply(lambda x : x.strftime(formatStringtod) )

# print(listingdf)