import json
import requests
import time
import configparser
import pytz
from datetime import datetime
import os
from dbutils import *
import logging
logger = logging.getLogger(__name__)

tz = pytz.timezone('America/Toronto')
formatString =  "%Y-%m-%dT%H:%M:%S.%f%z"
dbformatString = '%Y-%m-%d %H:%M%z'



absolute_path = os.path.dirname(os.path.abspath(__file__))
cacheLoc = f'{absolute_path}\\tmp\\cache.json'

def load_ingestion_listings():
    
    todayDate = datetime.today().astimezone(tz)
    today = todayDate.strftime(dbformatString)
    datestr = todayDate.strftime('%Y%m%d%H')
    logging.basicConfig(filename=f'{absolute_path}\\log\\ingestion_listing_{datestr}.log', level=logging.INFO)

    f = open(cacheLoc)
    cache = json.load(f)
    f.close()

    logger.info(f"getting data at {today}")
    res = None
    try:
        res = getData()["data"]
    except:
        logger.error("error retriving data from API")
        return
    resDict = {}

    for item in res:
        # splitting if conditions here, for future notification modifications
        if item["tType"] == "o" or item["tType"] == "os":
                setData(item, resDict , "o", today)  
        else:
                setData(item, resDict, "r", today) 

    resDict = filterCache(resDict, cache)



    logger.info(f"loading {len(resDict)} records...")
    # try:
    load_tbl("ingestion_shoptitans", "ingestion_listings", resDict)
    # except:
    #     logger.error("error loading data into DB")


    try:
        with open(cacheLoc, 'w') as outfile:
            json.dump(cache, outfile)
    except:
        logger.error("error writing cache files")



def getData():
    
    api_url = "https://smartytitans.com/api/item/last/all"
    response = requests.get(api_url).json()
    return response

def setData(item, dict, listType, today):

    tmpdict = {}
    id = item["uid"]
    quality = item["tag1"]
    # constrcut an id for cache filtering
    if quality is not None:
        id = id + "_" + quality
    
    if item["tag3"] == "freshspirit":
        id += "_fresh"
    
    id = id + "_" + listType

    timestamp = datetime.strptime(item["createdAt"], formatString)\
                .astimezone(pytz.timezone("US/Eastern")).replace(second=0, microsecond=0)
    date = int(timestamp.strftime("%Y%m%d"))
    tier = item["tier"]
    goldPrice = item["goldPrice"]
    if goldPrice is None:
        goldPrice = 0
    gemsPrice = item["gemsPrice"]
    if gemsPrice is None:
        gemsPrice = 0

    dict[id] = tmpdict
    tmpdict["uid"] = item["uid"]
    tmpdict["tType"] = listType
    tmpdict["tag1"] = quality
    tmpdict["tag2"] = item["tag2"]
    tmpdict["tag3"] = item["tag3"]
    tmpdict["goldQty"] = item["goldQty"]
    tmpdict["gemsQty"] = item["gemsQty"]
    tmpdict["tier"] = tier
    tmpdict["orders"] = item["order"]
    tmpdict["goldPrice"] = goldPrice
    tmpdict["gemsPrice"] = gemsPrice
    tmpdict["createdAt"] = timestamp
    tmpdict["retrievedAt"] = today
    tmpdict["yearmonday"] = date
    return


def filterCache(dict, cache):
    resDict = {}

    for key in dict:
        currItem = dict[key]
        currTime = currItem["createdAt"]

        #  if current item's updated time is equal to the same item's udpated time from last minute, skip this one

        if key in cache:
            if currTime <= datetime.strptime(cache[key]["createdAt"], dbformatString):
                continue
        
        # else update the createdTime in cache
        tmpDict = {}
        tmpDict ["createdAt"] = currTime.strftime(dbformatString)
        cache[key] = tmpDict 
        resDict[key] = currItem

    return resDict