import json
import requests
import time
import pandas as pd
import os 
import sys
from dbutils import *

# engine = psycopg2.connect(f"dbname='ingestion_shoptitans' user='postgres' host='127.0.0.1' port='5432' password='x'")
# df = pd.read_sql(f'select * from ingestion_items', con=engine)


def load_dim_item():
    df = tbl_to_df_pds("ingestion_shoptitans", "ingestion_items")
    # df["maxvalue"] = df["trademinmaxvalue"].str.split(";")
    # df["maxvalue"] = df["maxvalue"].apply(lambda x: x[1].split(",") if len(x) >= 2 else None)
    def generate_quality_maxvalue(row):
        
        if row["type"] == "meal" or row["type"] == "dessert":
            fresh = row["uid"] + '_fresh'
            row["uid"] = [row["uid"], fresh]
        
        if row["trademinmaxvalue"] is None:
            row["quality"] = ["none"]
            row["tradeMaxValue"] = [0]
        else :
            value_arr = row["trademinmaxvalue"].split(";")[1].split(",")
            if row["type"] == "component":
                row["quality"] = ["none"]
                row["tradeMaxValue"] = [int(value_arr[0])]
            else:
                row["quality"] = ["common", "uncommon", "flawless", "epic", "legendary"]
                row["tradeMaxValue"] = [int(x) for x in value_arr]
        return row

    df = df.apply(generate_quality_maxvalue, axis = 1)
    df = df.explode("uid")
    df = df.explode(["tradeMaxValue", "quality"])
    df["value125"] = df["tradeMaxValue"].apply(lambda x : x/10)
    df["code"] = df["uid"]
    df["uid"] = df.apply(lambda x: x["code"] if x["quality"] == "none" else x["code"] + "_" + x["quality"], axis = 1)
    string_to_int = lambda x: int(float(x) * 100) if x is not None else 0
    df["eva"] = df["eva"].apply(string_to_int)
    df["crit"] = df["crit"].apply(string_to_int)
    df = df.drop(columns = ["trademinmaxvalue",'u1req','u2req','u3req','u4req','u5req'])
    
    res = df.to_dict('records')
    load_tbl("dm_shoptitans", "dim_item", res)
    truncate_tbl("ingestion_shoptitans", "ingestion_items")
    return