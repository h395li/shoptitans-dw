import pandas as pd
from dbutils import *
import pytz
from datetime import datetime
import time


# engine = psycopg2.connect(f"dbname='ingestion_shoptitans' user='postgres' host='127.0.0.1' port='5432' password='x'")
# df = pd.read_sql(f'select * from ingestion_items', con=engine)
tz = pytz.timezone('America/Toronto')
dbformatString = '%Y-%m-%d %H:%M%z'
formatStringDate = '%Y%m%d'
formatStringtod = '%H%M'

def load_fact_offerlisting(date_to_load):
    t1 = time.time()
    listingdf = tbl_to_df_pds("ingestion_shoptitans", "ingestion_listings", yearmonday=date_to_load, tType='o')
    lookup_item = tbl_to_df_pds("dm_shoptitans", "dim_item")[['item_key', 'uid']]
    # date dim and tod
    # lookup_date = tbl_to_df_pds("dm_shoptitans", "dm_date")
    # lookup_tod = tbl_to_df_pds("dm_shoptitans", "dm_tod")

    listingdf = listingdf[['uid', 'tag1', 'tag3', 'goldqty', 'gemsqty', 'goldprice', 'gemsprice', 'createdat']]

    def rowTransformation(row):
        if row["tag3"] == "fresh":
            row["uid"] += "_fresh" 
        
        if row["tag1"] is None:
            row["uid"] += "_common"
        else :
            row["uid"] += '_' + row["tag1"]
        
        # createtime = datetime.strptime(row["createdAt"])
        row["date_key"] = row["createdat"].strftime(formatStringDate)
        row["tod_key"] = row["createdat"].strftime(formatStringtod)
        
        return row[["date_key", "tod_key", "uid", 'goldqty', 'gemsqty', 'goldprice', 'gemsprice']]
    listingdf = listingdf.apply(rowTransformation, axis=1)
    t2 = time.time()
    
    listingdf = listingdf.merge(lookup_item, left_on='uid', right_on='uid')
    # print(listingdf)
    
    # use spark partitionby will be more efficient
    excepdf = listingdf[listingdf["item_key"].isnull()]
    excepdf = excepdf.drop(columns = ["item_key"])
    listingdf = listingdf[listingdf["item_key"].notnull()]
    listingdf = listingdf.drop(columns = ["uid"])
    
    
    print(f"transformation took {t2 - t1} seconds")
    
    
    res = listingdf.to_dict('records')
    load_tbl("dm_shoptitans", "fact_offer_listings", res)
    excepres = excepdf.to_dict('records')
    load_tbl("dm_shoptitans", "fact_offer_listings_excep", excepres)
    print(f"load took {t3 - t2} seconds")
    return