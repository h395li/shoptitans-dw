import json
import requests
import time
import pandas as pd
import os 
import sys
from dbutils import *
    
absolute_path = os.path.dirname(os.path.abspath(__file__))
csv_path = f'{absolute_path}\\tmp\\itemData.csv'

extra_items = f'{absolute_path}\\tmp\\extra_items.json'
cacheLoc = f'{absolute_path}\\tmp\\cache_items.json'

typeDict = {
    "ws" : "sword", "wa" : "axe", "wd" : "dagger", "wm" : "mace", "wp" : "spear", "wb" : "bow", "ww" : "wand", "wt" : "staff", "wg" : "gun", "wc" : "crossbow", "wi" : "instrument", "ah" : "heavy armor", "am" : "light armor", "al" : "clothes", "hh" : "helmet", "hm" : "rogue hat", "hl" : "magician hat", "gh" : "gauntlets", "gl" : "gloves", "bh" : "heavy footwear",
    "bl" : "light footwear", "uh" : "herbal medicine", "up" : "potion", "us" : "spell", "xs" : "shield", "xr" : "ring", "xa" : "amulet", "xc" : "cloak", "xf" : "familiar", "xx" : "aurasong", "fm" : "meal", "fd" : "dessert", "xu" : "runestone", "xm" : "moonstone", "z" : "enchantments", "m" : "component"
}

removeComma = lambda x : int("".join(x.split(",")))


def getItemData(): 
    api_url = "https://smartytitans.com/assets/gameData/items.json"
    last = json.load(open(cacheLoc))
    response = requests.get(api_url).json()
    # response = {}
    extraList = []
    dumpDict = {}

    for item in response:
        if item in last:
            dumpDict[item] = last[item]
            continue
        tmpDict = {}
        dumpDict[item] = tmpDict
        extraList.append(tmpDict)
        tmpDict["uid"] = response[item]["uid"]
        tmpDict["type"] = typeDict.setdefault(response[item]["type"], response[item]["type"])
        tmpDict["xp"] = response[item]["xp"]
        tmpDict["craftXp"] = response[item]["craftXp"]
        tmpDict["tradeMinMaxValue"] = response[item]["tradeMinMaxValue"]
        tmpDict["value"] = response[item]["value"]
        tmpDict["favor"] = response[item]["favor"]
        tmpDict["time"] = response[item]["time"]
        tmpDict["atk"] = response[item]["atk"]
        tmpDict["def"] = response[item]["def"]
        tmpDict["hp"] = response[item]["hp"]
        tmpDict["eva"] = response[item]["eva"]
        tmpDict["crit"] = response[item]["crit"]
        tmpDict["excl"] = response[item]["excl"]
        tmpDict["tier"] = response[item]["tier"]
        tmpDict["worker1"] = response[item]["worker1"]
        tmpDict["worker2"] = response[item]["worker2"]
        tmpDict["worker3"] = response[item]["worker3"]
        tmpDict["resource1"] = response[item]["resource1"]
        tmpDict["r1Qty"] = response[item]["r1Qty"]
        tmpDict["resource2"] = response[item]["resource2"]
        tmpDict["r2Qty"] = response[item]["r2Qty"]
        tmpDict["resource3"] = response[item]["resource3"]
        tmpDict["r3Qty"] = response[item]["r3Qty"]
        tmpDict["component1"] = response[item]["component1"]
        tmpDict["c1Qty"] = response[item]["c1Qty"]
        tmpDict["c1Tags"] = response[item]["c1Tags"]
        tmpDict["component2"] = response[item]["component2"]
        tmpDict["c2Qty"] = response[item]["c2Qty"]
        tmpDict["c2Tags"] = response[item]["c2Tags"]
        tmpDict["u1Req"] = response[item]["u1Req"]
        tmpDict["u2Req"] = response[item]["u2Req"]
        tmpDict["u3Req"] = response[item]["u3Req"]
        tmpDict["u4Req"] = response[item]["u4Req"]
        tmpDict["u5Req"] = response[item]["u5Req"]
        tmpDict["upgrade1"] = response[item]["upgrade1"]
        tmpDict["upgrade2"] = response[item]["upgrade2"]
        tmpDict["upgrade3"] = response[item]["upgrade3"]
        tmpDict["upgrade4"] = response[item]["upgrade4"]
        tmpDict["upgrade5"] = response[item]["upgrade5"]
        tmpDict["supgrade1"] = response[item]["supgrade1"]
        tmpDict["supgrade2"] = response[item]["supgrade2"]
        tmpDict["supgrade3"] = response[item]["supgrade3"]
        tmpDict["suCost"] = response[item]["su1Cost"]
        tmpDict["elementAffinity"] = response[item]["elementAffinity"]
        tmpDict["spiritAffinity"] = response[item]["spiritAffinity"]
        tmpDict["discount"] = response[item]["discount"]
        tmpDict["surcharge"] = response[item]["surcharge"]
        tmpDict["suggest"] = response[item]["suggest"]
        tmpDict["speedup"] = response[item]["speedup"]
        tmpDict["releaseAt"] = response[item]["releaseAt"]


    with open(cacheLoc, "w") as outfile: 
        json.dump(dumpDict, outfile, indent=2)
        # outfile.write('\n')
    return extraList

def find_name(code, map):
    
    return map[code + "_name"]

def load_ingestion_items():
    
    extra_list = getItemData()
    if len(extra_list) == 0:
        # logger.info("no recent added items")
        return
    api_url = "https://smartytitans.com/assets/gameData/texts_en.json"
    response_en = requests.get(api_url).json()["texts"]
    api_url = "https://smartytitans.com/assets/gameData/texts_zh_cn.json"
    response_zh = requests.get(api_url).json()["texts"]
    itemdf = pd.json_normalize(extra_list)
    
    
    itemdf["name_en"] = itemdf["uid"].apply(find_name, args = (response_en,))
    itemdf["name_zh"] = itemdf["uid"].apply(find_name, args = (response_zh,))
    
    # print(itemdf)
    res = itemdf.to_dict('records')
    load_tbl("ingestion_shoptitans", "ingestion_items", res)
    return

