import json
import requests
import time
import pandas as pd
import os 
import sys
from dbutils import *
import datetime
import pytz


tz = pytz.timezone('America/Toronto')


def staticload_dm_date():
    formatString = '%Y%m%d'
    date = datetime.datetime(2024, 1, 1)
    rowlist = []

    for i in range(731): 
        currdate = date + datetime.timedelta(days=i)
        # on a larger project, date_key should be ameaningless surrogate key instead a meaningful integer
        # for this scale of project, I'm taking a short cut for the convenience of joining
        date_key = int(currdate.strftime(formatString))
        year = currdate.year
        month = currdate.month
        day = currdate.day
        eventday = i % 28
        event = "Passive Event"
        # this should be recalculated if the start date does not begin with Jan 1st, 2024
        if eventday <= 2 or eventday >= 24:
            event = "KC"
        elif eventday >=4 and eventday < 8:
            event = "TOT"
        elif eventday >=10 and eventday < 15:
            event = "DI"
        elif eventday >=18 and eventday < 22:
            event = "LCOG"
        dict = {'date_key' : date_key, 'year' : year, 'month' : month, 'day' : day, 'event' : event}
        rowlist.append(dict)       
    load_tbl("dm_shoptitans", "dim_date", rowlist)

def staticload_dm_tod():
    formatString = '%H%M'
    date = datetime.datetime(2024, 1, 1, 0, 0)
    rowlist = []
    for i in range(24):
        for j in range(60):
            diff = i * 60 + j
            currdate = date + datetime.timedelta(minutes=diff)
            
            # on a larger project, time of day key should be ameaningless surrogate key instead a meaningful integer
            # for this scale of project, I'm taking a short cut for the convenience of joining
            tod_key = int(currdate.strftime(formatString))
            hour = currdate.hour
            minute = currdate.minute
            daynight = "day"
            
            if hour < 11 or hour >= 23:
                daynight = "night"
            
            dict = {'tod_key' : tod_key, 'hour' : hour, 'minute' : minute, 'daynight' : daynight}
            rowlist.append(dict)
            
    load_tbl("dm_shoptitans", "dim_tod", rowlist)
    